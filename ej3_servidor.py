import sys
import socket
import random

if len(sys.argv) == 1:
	puerto=9999
else:
	puerto=int(sys.argv[1])
	
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", puerto))

while (1):
	datagrama, origen = s.recvfrom(1024)  
	if (random.randint(0,1) >= 0.5): 
		print "Se ha recibido un datagrama desde", origen
		print "Contiene lo siguiente:"
		print datagrama
	else:
		print "Simulando paquete perdido"
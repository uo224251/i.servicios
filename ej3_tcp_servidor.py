# -*- coding: utf-8 -*-

import sys
import socket
import time

def recvall(s, entero):
	total=""
	while entero!=0:
		retorno=s.recv(entero)
		recibidos= len(retorno)
		entero=entero-recibidos
		total=total+retorno
	return total


if len(sys.argv) == 1:
	puerto=9999
else:
	puerto=int(sys.argv[1])


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  

# crea un socket de tipo TCP

# Asignarle puerto
s.bind(("", puerto))

# Ponerlo en modo pasivo
s.listen(5)  

# Bucle principal de espera por clientes
while True:
	print "Esperando un cliente"
	sd, origen = s.accept()
	time.sleep(3)
	print "Nuevo cliente conectado desde %s, %d" % origen
	continuar = True
	while continuar:
		datos = sd.recv(80)
		if datos=="":  
			print "Conexi�n cerrada de forma inesperada por el cliente"
			sd.close()
			continuar = False
		else:
			print "Recibido mensaje: %s" % datos
			linea = datos[:-2]
			linea = linea[::-1]
			sd.sendall(linea+"\r\n")
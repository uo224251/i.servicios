import sys
import socket

if len(sys.argv) == 1:
	ip="localhost"
	puerto=9999
elif len(sys.argv)==2:
	puerto=9999
	ip = sys.argv[1]
else:
	ip=sys.argv[1]
	puerto=int(sys.argv[2])
	
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
i=1
while 1:
	datos="%d: " %i
	escrito=raw_input("Escriba un mensaje, FIN para terminar:")
	datos = datos + escrito
	print "\n"
	if (escrito == "FIN"):
		break
	i=i+1
	time=0.1
	while time<2:
		s.settimeout(time)
		try:
			s.sendto(datos, (ip, puerto))
			datagrama, origen = s.recvfrom(1024)
			if datagrama=="OK":
				print "Recibida confirmacion"
				break
			else:
				print "Recibido datagrama no esperado"
		except socket.timeout:
			print "ERROR. El datagrama de confirmacion no llega"
			time=time*2
		except:
			raise
	if(time>=2):
		print "Puede que el servidor este caido intentelo mas tarde"
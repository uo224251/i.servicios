import sys
import socket

if len(sys.argv) == 1:
	puerto=9999
else:
	puerto=int(sys.argv[1])
	
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", puerto))

while (1):
	datagrama, origen = s.recvfrom(1024)  
	print "Se ha recibido un datagrama desde", origen
	print "Contiene lo siguiente:"
	print datagrama
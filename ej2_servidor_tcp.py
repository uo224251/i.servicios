# -*- coding: utf-8 -*-

import sys
import socket


def recvall(s, entero):
	total=""
	while entero!=0:
		retorno=s.recv(entero)
		recibidos= len(retorno)
		entero=entero-recibidos
		total=total+retorno
	return total


if len(sys.argv) == 1:
	puerto=9999
else:
	puerto=int(sys.argv[1])


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  

# crea un socket de tipo TCP

# Asignarle puerto
s.bind(("", puerto))

# Ponerlo en modo pasivo
s.listen(5)  

# Bucle principal de espera por clientes
while True:
    print "Esperando un cliente"
    sd, origen = s.accept()
    print "Nuevo cliente conectado desde %s, %d" % origen
    continuar = True

    while continuar:
        datos = recvall(sd,5)
        if datos=="":  
            print "Conexión cerrada de forma inesperada por el cliente"
            sd.close()
            continuar = False
        elif datos=="FINAL":
            print "Recibido mensaje de finalización"
            sd.close()
            continuar = False
        else:
            print "Recibido mensaje: %s" % datos
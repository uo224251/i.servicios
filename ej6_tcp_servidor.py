# -*- coding: utf-8 -*-

import sys
import socket
import time

def recvall(s, entero):
	total=""
	while entero!=0:
		retorno=s.recv(entero)
		recibidos= len(retorno)
		entero=entero-recibidos
		total=total+retorno
	return total
def recibe_longitud(s):
	flag=True
	total=[]
	while flag:
		recibidos=s.recv(1)
		if recibidos == '\n':
			flag=False
			break
		total.append(recibidos)

	return "".join(total)

if len(sys.argv) == 1:
	puerto=9999
else:
	puerto=int(sys.argv[1])


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  

# crea un socket de tipo TCP

# Asignarle puerto
s.bind(("", puerto))

# Ponerlo en modo pasivo
s.listen(5)  

# Bucle principal de espera por clientes
while True:
	print "Esperando un cliente"
	sd, origen = s.accept()
	time.sleep(3)
	print "Nuevo cliente conectado desde %s, %d" % origen
	continuar = True
	while continuar:
		longitud = recibe_longitud(sd)   # Internamente usa bucle o readline
		mensaje = recvall(sd, int(longitud))
		if mensaje=="":  
			print "Conexi�n cerrada de forma inesperada por el cliente"
			sd.close()
			continuar = False
		else:
			print "Recibido mensaje: %s" % mensaje
			linea = mensaje[:-2]
			linea = linea[::-1]
			sd.sendall(linea+"\r\n")